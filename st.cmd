#!/usr/bin/env iocsh.bash

# All require need to have version number
require(julabof25hl)

# Set parameters when not using auto deployment
#epicsEnvSet(IPADDR,     "999.999.999.999")
#epicsEnvSet(IPPORT,     "1111")
epicsEnvSet(PREFIX,     "SES:JULABO-001")
epicsEnvSet(PORTNAME,   "$(PREFIX)")
epicsEnvSet(TEMPSCAN,   "1")
epicsEnvSet(CONFSCAN,   "10")
epicsEnvSet(SETPNAME,   "Highest_SP")
epicsEnvSet(SETPNR,     "11")
epicsEnvSet(LOCATION,   "SES; $(IPADDR)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(julabof25hl_DIR)db")

#Use for Lewis sim
epicsEnvSet(IPADDR, "127.0.0.1") #For use with Lewis simulator
epicsEnvSet(IPPORT, "9999")

#Specifying the TCP endpoint and port name
drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

#Load your database defining the EPICS records
iocshLoad("$(julabof25hl_DIR)/julabof25hl.iocsh", "P=$(PREFIX), R=:, PORT=$(PORTNAME), ADDR=$(IPPORT)")
